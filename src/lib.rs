mod error;
mod traits;

pub use error::CrudError;
pub use rocket_crud_derive::CRUD;
pub use traits::TypeInfo;
pub use traits::GetId;
