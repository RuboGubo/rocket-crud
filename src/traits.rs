use rocket_db_pools::Database;

use sqlx::Executor;

pub trait GetId<ID> {
    fn get_id(&self) -> ID;
}

pub trait TypeInfo {
    type ID;
    type Conn<'a>: Executor<'a>;
    type DB: Database;
}
