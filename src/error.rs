use rocket::{http::Status, response::{self, Responder}, Request, Response};
use thiserror::Error;


#[derive(Debug, Error)]
pub enum CrudError {
    #[error("There was a problem in the Database: {0}")]
    DatabaseError(#[from] sqlx::Error),
    #[error("Could not find value")]
    NotFound,
}


impl<'r> Responder<'r, 'static> for CrudError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let string = self.to_string();

        Response::build_from(string.respond_to(req)?)
            .status(Status::NotFound)
            .ok()
    }
}
