use proc_macro::TokenStream;
use quote::{format_ident, quote};

#[proc_macro_derive(CRUD)]
pub fn url_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).expect("Failed to parse Token Stream");

    // Build the trait implementation
    impl_url_get(&ast)
}

fn impl_url_get(ast: &syn::DeriveInput) -> TokenStream {
    let struct_ident = &ast.ident;
    let struct_str = format_ident!("{}", struct_ident);

    let faring_name = format!("Routes for {}", struct_str);
    let url = format!("/{}", struct_str);

    let gen = quote! {
        use rocket::{
            serde::json::Json,
            http::Status,
            routes,
            Route,
            Build,
            delete,
            get,
            post,
            put,
            Rocket,
            response::status,
        };
        use sqlx::Acquire;
        use rocket_db_pools::Connection;
        use sqlx_crud::Crud;
        use rocket_crud::{CrudError};
        use rocket::fairing::{
            self,
            Fairing,
            Kind,
            Info,
        };

        /// URL  `GET` `/<id>`
        #[get("/<id>")]
        pub async fn get(mut conn: Connection<<#struct_ident as TypeInfo>::DB>, id: <#struct_ident as TypeInfo>::ID) -> Result<Json<#struct_ident>, CrudError> {
            match #struct_ident::by_id(conn.acquire().await?, id).await {
                Ok(Some(x)) => Ok(Json(x)),
                Ok(None) => Err(CrudError::NotFound),
                Err(x) => Err(x.into()),
            }
        }

        /// URL  `POST` `/`;
        /// Body `Json<Type>`
        ///
        /// Will ignore the id provided, and return the object with the new id.
        #[post("/", data = "<json>")]
        pub async fn new(mut conn: Connection<<#struct_ident as TypeInfo>::DB>, json: Json<#struct_ident>,) -> Result<Json<#struct_ident>, CrudError> {
            let connection = conn.acquire().await?;
            Ok(Json(
                json.into_inner()
                    .create(connection)
                    .await?
            ))
        }

        /// URL  `PUT` `/`;
        /// Body `Json<Type>`
        ///
        /// Will replace the values at that id with the ones provided, partial updates are NOT supported
        #[put("/", data = "<json>")]
        pub async fn update(mut conn: Connection<<#struct_ident as TypeInfo>::DB>, json: Json<#struct_ident>,) -> Result<Json<#struct_ident>, CrudError> {
            // Ok(Json(
            //     json.into_inner()
            //         .create_db(conn)
            //         .await
            //         .map_err(|x| status::NotFound(x.to_string()))?

            // ))
            todo!()
        }

        /// URL  `DELETE` `/<id>`
        #[delete("/<id>")]
        pub async fn delete(mut conn: Connection<<#struct_ident as TypeInfo>::DB>, id: <#struct_ident as TypeInfo>::ID) -> Result<(), CrudError> {
            Ok(
                #struct_ident::delete_by_id(id, conn.acquire().await?)
                    .await?
            )
        }

        #[rocket::async_trait]
        impl Fairing for #struct_ident
        {
            fn info(&self) -> Info {
                Info {
                    name: #faring_name,
                    kind: Kind::Ignite,
                }
            }

            async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
                Ok(rocket.mount(#url, routes![get, new, delete, update]))
            }
        }
    };

    let gen = quote! {  #gen  };

    gen.into()
}
